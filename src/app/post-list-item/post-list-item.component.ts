import { Component, OnInit, Input } from '@angular/core';
import {Post} from '../post.model';
import { PostService } from '../post.service';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.css']
})
export class PostListItemComponent implements OnInit {

  @Input() post: Post;

  ngOnInit() {
  }

  onLike() {
    this.postService.addLoveIt(this.post.id);
  }

  onDislike() {
    this.postService.removeLoveIt(this.post.id);
  }

  onDelete() {
    this.postService.deletePost(this.post.id);
  }

  constructor(private postService: PostService) {}

}

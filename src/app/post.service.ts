import { Injectable } from '@angular/core';
import { Post } from './post.model';
import { Subject } from 'rxjs';
import { ConcatSource } from 'webpack-sources';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  postsSubject = new Subject<Post[]>();

  posts: Post[] = [
    new Post(0, 'titre 1', 'blablabla', 2),
    new Post(1, 'Un autre titre', 'encore du contenu', -1),
    new Post(2, 'La saviez vous ?', 'La personne lisant ceci est génial !', 0),

  ];

  nextId = 3;

  emitPostsSubject() {
    this.postsSubject.next(this.posts.slice());
  }

  addPost(titre: string, contenu: string) {

    const newPost = new Post(this.nextId, titre, contenu, 0);
    this.nextId++;

    this.posts.push(newPost);
    this.emitPostsSubject();
  }

  deletePost(id: number) {

    const index = this.findPostIndexById(id);
    this.posts.splice(index, 1);
    this.emitPostsSubject();

  }

  addLoveIt(id: number) {
    const index = this.findPostIndexById(id);
    this.posts[index].loveIts++;
  }

  removeLoveIt(id: number) {
    const index = this.findPostIndexById(id);
    this.posts[index].loveIts--;
  }

  findPostIndexById(id: number): number {
    for (let i = 0; i < this.posts.length; i++) {
      if (this.posts[i].id === id) {
        return i;
      }
    }
    console.log('Error: pas de post trouvé avec cet ID: ' + id);
  }

}
